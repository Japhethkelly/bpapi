<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    //

    public function checkBalance(Request $request){
    	$user= \App\Subscriber::where('isDeleted','0')->where('status','Active')->where('id',$request->AshesiID)->get();
    	//echo $request;

    if (!$user->isEmpty()) {
        // use is found
    		foreach ($user as $key) {

        $response["error"] = FALSE;
        $response["AshesiID"] = $key->id;
        $response["user"]["firstname"] = $key->firstname;
        $response["user"]["lastname"] = $key->lastname;
        $response["user"]["balance"] = $key->balance;
        
    	}
        echo json_encode($response);
    } else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "User credentials not found!";
        echo json_encode($response);
    }

    }
}
