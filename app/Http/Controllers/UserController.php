<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
     public function login2(Request $request){
    	$user= \App\User::where('isDeleted','0')->where('status','Active')->where('email',$request->email)->where('password',$request->password)->get();
    	
    	if (!$user->isEmpty()) {
        // use is found
    		foreach ($user as $key) {
    		# code...
    		$response["error"] = FALSE;
        $response["ConductorId"] = $key->id;
        $response["user"]["firstname"] = $key->firstname;
        $response["user"]["lastname"] = $key->lastname;
        $response["user"]["password"] = $key->password;
        $response["user"]["email"] = $key->email;
        $response["user"]["created_at"] = $key->created_at;
    	}
        echo json_encode($response);
    } else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "Login credentials are wrong. Please try again!";
        echo json_encode($response);
    }
    	
}
}
