<?php

namespace App\Http\Controllers;

use App\Subscriber;
use App\Transaction;
use App\Audit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function transact(Request $request){
    	
        $trans = new Transaction;
        $trans->subscriberId = $request->AshesiID;
        $trans->transactionType = 'Payment';
        $trans->Amount = $request->Amount;
        $trans->userId = $request->userID;
        $trans->routeId = $request->routeID;

        $trans->save();


    	$up = Subscriber::find($request->AshesiID);
		$up->balance=$request->newBalance;
		$up->save();

		DB::table('audits')->insert([
    		'subscriberId'=>$request->AshesiID,
    		'delta'=>$request->Amount,
    		'transactionId'=>$trans->id,
    		'userId'=>$request->userID,
    		'oldBalance'=>$request->oldBalance,
    		'newBalance'=>$request->newBalance    		
    		]);
    	//echo $request;
    	$response["error"] = FALSE;
        $response["response"] = "Transaction Successfully Processed";
        echo json_encode($response);
}


}
