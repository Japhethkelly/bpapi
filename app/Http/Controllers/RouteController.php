<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteController extends Controller
{
    //
    public function index(){
    	$route= \App\Route::where('isDeleted','0')->where('status','Active')->get();
    	return json_encode($route);
    }
}
