<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BusController extends Controller
{
    public function index(){
    	$bus= \App\Bus::where('isDeleted','0')->where('status','Active')->get();
    	return json_encode($bus);
    }
}
