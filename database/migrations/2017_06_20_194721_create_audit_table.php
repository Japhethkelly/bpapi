<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subscriberID');
            $table->integer('transactionID')->unsigned();
            $table->integer('userID')->unsigned();
            $table->decimal('oldBalance',5,2);
            $table->decimal('newBalance',5,2);
            $table->decimal('delta',5,2);
            $table->string('details')->nullable();
            
            $table->boolean('isVerified');
            $table->timestamps();

            $table->foreign('userID')->references('id')->on('users');
            $table->foreign('transactionID')->references('id')->on('transactions');
            $table->foreign('subscriberID')->references('id')->on('subscribers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audits');
    }
}
