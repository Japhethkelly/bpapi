<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subscriberID');
            $table->decimal('amount',5,2);
            $table->enum('transactionType',['Topup', 'Payment', 'Reload']);
            $table->integer('userID')->unsigned();
            $table->integer('routeID')->unsigned();
            $table->timestamps();

            $table->foreign('userID')->references('id')->on('users');
            $table->foreign('routeID')->references('id')->on('routes');
            $table->foreign('subscriberID')->references('id')->on('subscribers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
