<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
        $table->string('id')->unique();             
        $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->decimal('balance',5,2);
            $table->string('assignedTo');
            $table->enum('status',['Active', 'Inactive']);
            $table->boolean('isDeleted');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribers');
    }
}
